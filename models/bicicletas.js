var mongoose=require("mongoose")
var Schema=mongoose.Schema
/*
    →Los modelos forman los documentos
    →Todos los modelos heredan de schema
    →Para acceder a los metodos del modelo se puede 
    usar statics para acceder ya sea desde la instacia
    o desde la clase; y methods solo desde la instancia
*/
var bicicletaSchema=new Schema({
    code:Number,
    color:String,
    modelo:String,
    ubicacion:{type:[Number],index:{type:"2dsphere",sparse:true}}
})
bicicletaSchema.statics.createInstance=function(code,color,modelo,ubicacion){
    return new this({
        code:code,
        color:color,
        modelo:modelo,
        ubicacion:ubicacion
    })
}
bicicletaSchema.statics.allBicicletas=function(cb){
    return this.find({},cb)
}
bicicletaSchema.statics.add=function(bicicleta,cb){
    this.create(bicicleta,cb)
}
bicicletaSchema.statics.findByCode=function(code,cb){
    return this.findOne({code:code},cb)
}
bicicletaSchema.statics.removeByCode=function(code,cb){
    return this.deleteOne({code:code},cb)
}
module.exports=mongoose.model("Bicicleta",bicicletaSchema)