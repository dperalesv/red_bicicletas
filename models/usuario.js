var mongoose=require("mongoose")
var Reserva=require("./reserva")
var Schema=mongoose.Schema

var usuarioSchema=new Schema({
    nombre:String
})

usuarioSchema.methods.reservar=function(biciId,desde,hasta,cb){
    var reserva=new Reserva({desde:desde, hasta:hasta,bicicleta:biciId,usuario:this._id })
    console.log(reserva)
    reserva.save(cb)
}

module.exports=mongoose.model("Usuario",usuarioSchema)
