# Aplicacion de red de bicicletas
Semana 1: Se ha desarrolla una aplicacion con CRUD en memoria y una API

## Requisitos 
Todo lo que necesitas para tener una copia de este proyecto en tu computadora ♥.
#### Tener Node.js instalado: https://nodejs.org/es/download/
#### Tener Git instalado: https://git-scm.com/downloads y tener una cuenta


## Para Ejecutar
1. Descargar el proyecto de: https://bitbucket.org/dperalesv/red_bicicletas/src 
2. Ir al lugar donde esta el proyecto descargado
3. Dar click derecho y abrir el git bush
4. Escribir el comando: npm run devstart
5. Abrir cualquier navegador, recomendable Google Chrome
6. Escribir localhost:3000 para acceder a la pagina principal y localhost:3000/bicicletas para acceder al CRUD de bicicletas

Semana 2: Aplicacion con base de datos(MongoDB) 
Ejecución de la API bicicleta
Api para crear bicicleta
![alt text](public/images/development/api/apiCreateBicicly.png)
Api para obtener bicicletas
![alt text](public/images/development/api/apiGetBicicly.png)
## Autor
[EmersonPV]
