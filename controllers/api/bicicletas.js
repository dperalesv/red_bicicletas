var Bicicleta=require("../../models/bicicletas.js")

exports.bicicleta_list=function(req,res){
    Bicicleta.allBicicletas(function(err,bicicletas){
        res.status(200).json({bicicletas:bicicletas})
    })
}
exports.bicicleta_create=function(req,res){
    var bicicleta= Bicicleta.createInstance(req.body.code,
        req.body.color,req.body.modelo,
        [req.body.lat,req.body.lng])
    
    Bicicleta.add(bicicleta,function(err,bicicletaCreated){
        res.status(200).json({bicicleta: bicicletaCreated})
    })
}


/*

exports.bicicleta_delete=function(req,res){
    Bicicleta.remove(req.body.id)
    res.status(200).send()
}
exports.bicicleta_update=function(req,res){
    var bicicleta=Bicicleta.findById(req.params.id)
    if(bicicleta){
        bicicleta.id=req.body.id
        bicicleta.color=req.body.color
        bicicleta.modelo=req.body.modelo
        bicicleta.ubicacion=[req.body.lat,req.body.lng]
        res.status(200).json({
            bicicleta: bicicleta
        })
    }else{
        res.status(500).send()
    }
    
}
exports.bicicleta_list=function(req,res){
    res.status(200).json({
        bicicletas: Bicicleta.allBicicletas
    })
}

exports.bicicleta_create=function(req,res){
    var bicicleta=new Bicicleta(req.body.id,
        req.body.color,req.body.modelo,
        [req.body.lat,req.body.lng])
    Bicicleta.add(bicicleta)
    res.status(200).json({
        bicicleta: bicicleta
    })
}
*/