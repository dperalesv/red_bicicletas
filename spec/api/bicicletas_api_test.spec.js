const Bicicleta=require("../../models/bicicletas")
const mongoose=require("mongoose")
const server=require("../../bin/www")
const request=require("request")

const url="http://localhost:3000/api/bicicletas"

describe("Testing API bicycle",()=>{
    beforeEach((done)=>{
        var mongoDB="mongodb://localhost/testdb"
        mongoose.connect(mongoDB,{useNewUrlParser:true})
        const db=mongoose.connection
        
        db.on("error",console.error.bind(console,"CONNECTION ERROR"))
        db.once("open",function(){
            console.log("WE ARE CONECTED TO DATA BASE ")
            done()
        })
    })  
    afterEach((done)=>{
        Bicicleta.deleteMany({},function(err,success){
            if(err) console.log(err)
            done()
        })
    })
    describe("GET BICICLETA /",()=>{
        it("Status 200",(done)=>{
            request.get(url,function(err,res,body){
                var result=JSON.parse(body)
                expect(res.statusCode).toBe(200)
                console.log(result)
                expect(result.bicicletas.length).toBe(0)
                done()
            })
        })
    })
    describe("POST BICICLETAS /create",()=>{
    	it("Status 200",(done)=>{
            var headers={"content-type":"application/json"}
            var bicicleta='{"code":10,"color":"negro","modelo":"montañesa","lat": -34, "lng": -54}'
            request.post({
                headers:headers,
                url:url+"/create",
                body:bicicleta
            },function(err,res,body){
                expect(res.statusCode).toBe(200)
                var bicicletaPost=JSON.parse(body).bicicleta
                console.log(bicicletaPost)
                expect(bicicletaPost.color).toBe("negro")
                console.log(JSON.parse(body))
                done()
            })
	    })
    })
})

/*

describe("POST BICICLETAS /create",()=>{
    	it("Status 200",(done)=>{
            var headers={"content-type":"application/json"}
            var bicicleta='{"code":10,"color":"negro","modelo":"montañesa"}'
            request.post({
                headers:headers,
                url:url+"/create",
                body:bicicleta
            },function(err,res,body){
                expect(res.statusCode).toBe(200)
                var bicicletaPost=JSON.parse(body)
                console.log(bicicletaPost)
                //expect(bicicletaPost.bicicleta.color).toBe("negro")
                done()
            })
	    })
    })
    






This comment belong to the first development, it was just worked 
on memory not using data base

const server=require("../../bin/www")
const request=require("request")
const Bicicleta=require("../../models/bicicletas")
var a=new Bicicleta(11,"negro","urbana",[-6.771485,  -79.837248])
Bicicleta.add(a)
//beforeEach(()=>{Bicicleta.allBicicletas=[]})
describe("GET bicicletas /",()=>{
    it("status 200",(done)=>{
        //expect(Bicicleta.allBicicletas.length).toBe(0)
        var a=new Bicicleta(10,"negro","urbana",[-6.771485,  -79.837248])
        Bicicleta.add(a)
        request.get("http://localhost:3000/api/bicicletas",(error,response,body)=>{
            expect(response.statusCode).toBe(200)
            done()
        })
    }) 
})

describe("PUT bicicleta /:id/update",()=>{
    it("status 200",(done)=>{
        var headers={"content-type":"application/json"}
        var bicicletaPut='{"id": 100,"color": "blanco","modelo": "urbana","lat": 5, "lng": 10}'      
        request.put({
            headers: headers,
            url: "http://localhost:3000/api/bicicletas/10/update",
            body: bicicletaPut
        },(error,response,body)=>{
            expect(response.statusCode).toBe(200)
            done()
        })
    })
})

describe("POST bicicletas /create",()=>{
    it("status 200",(done)=>{
        var headers={"content-type":"application/json"}
        var bicicleta='{"id": 10,"color": "negro oscurooo","modelo": "montañesa","lat": -34.597889, "lng": -58.383556}'      
        request.post({
            headers: headers,
            url: "http://localhost:3000/api/bicicletas/create",
            body: bicicleta
        },(error,response,body)=>{
            expect(response.statusCode).toBe(200)
            expect(Bicicleta.findById(10).color).toBe("negro oscurooo")
            done()
        })
    })
})
describe("DEL bicicleta /delete",()=>{
    it("status 200",(done)=>{
        var id='{"id":11}'
        var headers={"content-type":"application/json"}
        request.del({
            headers:headers,
            url: "http://localhost:3000/api/bicicletas/delete",
            body: id
        },(error,response,body)=>{
            expect(response.statusCode).toBe(200)
            done()
        })
    })
})

*/