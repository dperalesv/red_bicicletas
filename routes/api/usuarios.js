var express=require("express")
var router=express.Router()
var usuarioController=require("../../controllers/api/usuarios")

router.get("/",usuarioController.usuarios_list)
router.post("/create",usuarioController.usuarios_create)
router.post("/reserva",usuarioController.usuarios_reservar)

module.exports=router