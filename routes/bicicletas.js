const express=require("express")
const router=express.Router()
var bicicletasController=require("../controllers/bicicletas")

router.get("/",bicicletasController.bicicleta_list)

router.get("/create",bicicletasController.bicicleta_create_get)
router.post("/create",bicicletasController.bicicleta_create_post)

router.post('/:id/remove',bicicletasController.bicicleta_remove)

router.get("/:id/modify",bicicletasController.bicicleta_modify_get)
router.post("/:id/modify",bicicletasController.bicicleta_modify_post)

module.exports=router