/*
    ejecutar npm jasmin o con
    jasmine spec/models/bicic
*/

const Bicicleta=require("../../models/bicicletas")
const mongoose=require("mongoose")

describe("Testing bicycle",()=>{
    beforeEach((done)=>{
        var mongoDB="mongodb://localhost/testdb"
        mongoose.connect(mongoDB,{useNewUrlParser:true})
        const db=mongoose.connection
        
        db.on("error",console.error.bind(console,"CONNECTION ERROR"))
        db.once("open",function(){
            console.log("WE ARE CONECTED TO DATA BASE ")
            done()
        })
    })  
    afterEach((done)=>{
        Bicicleta.deleteMany({},function(err,success){
            if(err) console.log(err)
            done()
        })
    })
    describe("Bicicleta.createInstance",()=>{
        it("crea una instancia de Bicicleta",()=>{
            var bicicleta=Bicicleta.createInstance(1,"negro","montañesa",[0,0])
            expect(bicicleta.code).toBe(1)
            expect(bicicleta.color).toBe("negro")
            expect(bicicleta.modelo).toBe("montañesa")
            expect(bicicleta.ubicacion[0]).toBe(0)
            expect(bicicleta.ubicacion[1]).toBe(0)
        })
    })
    describe("Bicicleta.allBicicletas",()=>{
        it("comienza vacia",(done)=>{
            Bicicleta.allBicicletas(function(err,bicicletas){
                expect(bicicletas.length).toBe(0)
                done()
            })
        })
    })
    describe("Bicicleta.add",()=>{
        it("agrega solo una bicicleta",(done)=>{
            var bicicleta=new Bicicleta({code:1,color:"verde",modelo:"urbana"})
            Bicicleta.add(bicicleta,function(err,Nbicicleta){
                if(err) console.log(err)
                Bicicleta.allBicicletas(function(err,bicicletas){
                    expect(bicicletas.length).toBe(1)
                    expect(bicicletas[0].code).toBe(Nbicicleta.code)
                    done()
                })
            })
        })
    })
    describe("Bicicleta.findByCode",()=>{
        it("Debe devolver la bicicleta con code 1",(done)=>{
            Bicicleta.allBicicletas(function(err,bicicletas){
                expect(bicicletas.length).toBe(0)
                var bicicleta=new Bicicleta({code:1,color:"negro"})
                Bicicleta.add(bicicleta,function(err,bicicleta){
                    if(err) console.log(err)
                    var bicicleta2=new Bicicleta({code:2,color:"blanco"})
                    Bicicleta.add(bicicleta2,function(err,bicicleta2){
                        if(err) console.log(err)
                        Bicicleta.findByCode(1,function(err,targetBicicleta){
                            expect(targetBicicleta.code).toBe(bicicleta.code)
                            expect(targetBicicleta.color).toBe(bicicleta.color)
                            done()
                        })
                    })
                })
            })
        })
    })
})



/*
const Bicicleta=require("../../models/bicicletas")

beforeEach(()=>{Bicicleta.allBicicletas=[]})
describe("Bicicleta.allBicicletas", ()=>{
    it("comienza vacia",()=>{
        expect(Bicicleta.allBicicletas.length).toBe(0)
    })
}) 

describe("Bicicleta.add",()=>{
    it("Agregamos bicicleta",()=>{
        expect(Bicicleta.allBicicletas.length).toBe(0)
        var a=new Bicicleta(1,"negro","urbana",[-6.771485,  -79.837248])
        Bicicleta.add(a)
        expect(Bicicleta.allBicicletas.length).toBe(1)              
        expect(Bicicleta.allBicicletas[0]).toBe(a)
    })
})

describe("Bicicleta.remove",()=>{
    it("Eliminamos bicicleta con id 1",()=>{
        expect(Bicicleta.allBicicletas.length).toBe(0)
        var a=new Bicicleta(1,"negro","urbana",[-6.771485,  -79.837248])
        Bicicleta.add(a)
        expect(Bicicleta.allBicicletas[0]).toBe(a)
        Bicicleta.remove(1)
        expect(Bicicleta.allBicicletas.length).toBe(0)
    })
})

describe("Bicicleta.findById",()=>{
    it("Debe devolver bicicleta con id 1",()=>{
        expect(Bicicleta.allBicicletas.length).toBe(0)
        var b=new Bicicleta(1,"azul","montañosa",[-6.771821, -79.839345])
        var c=new Bicicleta(3,"blanco","urbana",[-6.771262,  -79.839251])
        Bicicleta.add(b)
        Bicicleta.add(c)
        var targetBici=Bicicleta.findById(1)
        expect(targetBici.id).toBe(1)
        expect(targetBici.color).toBe(b.color)
        expect(targetBici.modelo).toBe(b.modelo)
    })
})
*/