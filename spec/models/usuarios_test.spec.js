const Bicicleta=require("../../models/bicicletas")
const Usuario=require("../../models/usuario")
const Reserva=require("../../models/reserva")
const mongoose=require("mongoose")

describe("Testing users",()=>{
    beforeEach((done)=>{
        var mongoDB="mongodb://localhost/testdb"
        mongoose.connect(mongoDB,{useNewUrlParser:true})
        const db=mongoose.connection
        
        db.on("error",console.error.bind(console,"CONNECTION ERROR"))
        db.once("open",()=>{
            console.log("WE ARE CONECTED TO DATA BASE ")
            done()
        })
    })  
    afterEach(function(done){
        Reserva.deleteMany({},function(err,succ){
            if(err) console.log(err)
            Usuario.deleteMany({},function(err,succ){
                if(err) console.log(err)
                Bicicleta.deleteMany({},function(err,succ){
                    if(err) console.log(err)
                    done()
                })
            })
        })
    })
    describe("Cuando un Usuario reserva una bicicleta",()=>{
        it("debe existir la reserva",(done)=>{
            const usuario=new Usuario({nombre:"Emerson"})
            usuario.save()
            const bicicleta=new Bicicleta({code:1,color:"negro",modelo: "urbana"})
            bicicleta.save()
            
            var today=new Date()
            var tomorrow=new Date()
            tomorrow.setDate(today.getDate()+1)
            usuario.reservar(bicicleta.id,today,tomorrow,function(err,reserva){
                Reserva.find({}).populate("bicicleta").populate("usuario").exec(function(err,reservas){
                    console.log(reservas[0])
                    expect(reservas.length).toBe(1)
                    expect(reservas[0].diasReserva()).toBe(2)
                    expect(reservas[0].bicicleta.code).toBe(1)
                    expect(reservas[0].usuario.nombre).toBe(usuario.nombre)
                    done()
                })
            })
        })
    })
})