var map = L.map('main_map').setView([-6.771581, -79.838517], 13)

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map)

$.ajax({
    datatype: "json",
    url: "api/bicicletas",
    success: function(result){
        console.log(result)
        result.bicicletas.forEach(function(bicicleta){
            L.marker(bicicleta.ubicacion, {title: bicicleta.id + bicicleta.modelo}).addTo(map)
        });
    }
})