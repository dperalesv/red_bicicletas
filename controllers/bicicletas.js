var Bicicleta=require("../models/bicicletas.js")

exports.bicicleta_list=function(req,res){
    res.render("bicicletas/index",{bicicletas: Bicicleta.allBicicletas})
}
exports.bicicleta_create_get=function(req,res){
    res.render("bicicletas/create")
}
exports.bicicleta_create_post=function(req,res){
    var bicicleta=new Bicicleta(req.body.id,
        req.body.color,req.body.modelo,
        [req.body.lat,req.body.lng])
    Bicicleta.add(bicicleta)
    res.redirect("/bicicletas")
}
exports.bicicleta_remove=function(req,res){
    Bicicleta.remove(req.body.id)
    res.redirect("/bicicletas")
}
exports.bicicleta_modify_get=function(req,res){
    var bicicleta=Bicicleta.findById(req.params.id)
    res.render("bicicletas/modify",{bicicleta})
}
exports.bicicleta_modify_post=function(req,res){
    var bicicleta=Bicicleta.findById(req.params.id)
    bicicleta.id=req.body.id
    bicicleta.color=req.body.color
    bicicleta.modelo=req.body.modelo
    bicicleta.ubicacion=[req.body.lat,req.body.lng]
    res.redirect("/bicicletas")
}